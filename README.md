# Binar: Authentication & Bcrypt

Di dalam repository ini terdapat implementasi authentication dan penggunaan `bcryptjs` di dalamnya.

## Screenshoot

### Running On Postman (Register)

![alt text](register-success.jpg)

### Running On Postman (login)

![alt text](login-success.jpg)

### Configuration On Database (postgres)

![alt text](insertdata-pgAdmin-success.jpg)

### Running On Postman (whoami)

![alt text](whoami-success.jpg)

## Getting Started

Untuk mulai membuat sebuah implementasi dari HTTP Server, mulainya menginspeksi file [`app/index.js`](./app/index.js), dan lihatlah salah satu contoh `controller` yang ada di [`app/controllers/mainController.js`](./app/controllers/mainController.js)

Lalu untuk menjalankan development server, kalian tinggal jalanin salah satu script di package.json, yang namanya `develop`.

```sh
yarn develop
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `yarn db:create` digunakan untuk membuat database
- `yarn db:drop` digunakan untuk menghapus database
- `yarn db:migrate` digunakan untuk menjalankan database migration
- `yarn db:seed` digunakan untuk melakukan seeding
- `yarn db:rollback` digunakan untuk membatalkan migrasi terakhir

## Endpoints

Di dalam repository ini, terdapat dua endpoint utama, yaitu `login` dan `register`.

##### `/api/v1/login`

Endpoint ini digunakan untuk login

###### Request

```json
{
  "email" : "dheka.mersandi@gmail.com",
    "password" : "dheka17"
}
```

###### Response

```json
{
    "id": 1,
    "email": "dheka.mersandi@gmail.com",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJkaGVrYS5tZXJzYW5kaUBnbWFpbC5jb20iLCJjcmVhdGVkQXQiOiIyMDIyLTA1LTE3VDIwOjQ4OjAxLjMxOVoiLCJ1cGRhdGVkQXQiOiIyMDIyLTA1LTE3VDIwOjQ4OjAxLjMxOVoiLCJpYXQiOjE2NTI4MjA1ODF9.-gL_shOm_c36xl-w1rREMUkPbYG1jFIo24cMWz4qFtc",
    "createdAt": "2022-05-17T20:48:01.319Z",
    "updatedAt": "2022-05-17T20:48:01.319Z"
}
```

##### `/api/v1/register`

Endpoint ini digunakan untuk registrasi user

###### Request

```json
{
  "email" : "dheka.mersandi@gmail.com",
    "password" : "dheka17"
}
```

###### Response

```json
{
    "id": 1,
    "email": "dheka.mersandi@gmail.com",
    "createdAt": "2022-05-17T20:48:01.319Z",
    "updatedAt": "2022-05-17T20:48:01.319Z"
}
```
